package com.wistar.service.impl;

import com.wistar.constant.LoginConstant;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        String loginType = requestAttributes.getRequest().getParameter("login_type");// 区分[后台管理人员/普通用户]登录
        if (StringUtils.isEmpty(loginType)){
            throw new AuthenticationServiceException("登录用户类型不能为空");
        }
        UserDetails userDetails = null;
        try {
            // 授权类型为refresh_token时，进行纠正
            String grantType = requestAttributes.getRequest().getParameter("grant_type");
            if (LoginConstant.REFRESH_TOKEN.equals(grantType)){
                username = adjustUsername(username, loginType);
            }
            switch (loginType){
                case LoginConstant.ADMIN_TYPE:
                    userDetails = loadSysUserByUsername(username);
                    break;
                case LoginConstant.MEMBER_TYPE:
                    userDetails = loadMemberUserByUsername(username);
                    break;
                default:
                    throw new AuthenticationServiceException("暂不支持的登录方式: "+loginType);
            }
        }catch (IncorrectResultSizeDataAccessException e){// 我们的用户不存在
            throw new UsernameNotFoundException("不存在的用户名："+username);
        }
        return userDetails;
    }

    /**
     * 纠正用户名
     * @param username 用户id
     * @param loginType admin_type、member_type
     * @return
     */
    private String adjustUsername(String username, String loginType) {
        if (LoginConstant.ADMIN_TYPE.equals(loginType)){
            // 管理员的纠正方式
            return jdbcTemplate.queryForObject(LoginConstant.QUERY_ADMIN_WITH_ID_SQL, String.class, username);
        }
        if (LoginConstant.MEMBER_TYPE.equals(loginType)){
            // 普通用户的纠正方式
            return jdbcTemplate.queryForObject(LoginConstant.QUERY_MEMBER_WITH_ID_SQL, String.class, username);
        }
        return username;
    }

    /**
     * 后台管理人员登录
     */
    private UserDetails loadSysUserByUsername(String username) {
        // 1.使用用户名查询用户
        return (UserDetails) jdbcTemplate.queryForObject(LoginConstant.QUERY_ADMIN_SQL, new RowMapper<Object>(){
            @Override
            public Object mapRow(ResultSet rs, int i) throws SQLException {
                if (rs.wasNull()){
                    throw new UsernameNotFoundException("不存在的用户名："+username);
                }
                long id = rs.getLong("id");// 用户id
                String password = rs.getString("password");// 用户id
                int status = rs.getInt("status");// 用户id
                return new User(
                        String.valueOf(id),
                        // 这里返回响应的password需要是已加密过的，否则报错：Encoded password does not look like BCrypt。
                        // 应改为：encode之后保存到数据库，而不是数据库取出再encode。
                        passwordEncoder.encode(password),
                        status == 1,
                        true,
                        true,
                        true,
                        getSysUserPermission(id)
                );
            }
        },username);
    }

    // 2.查询用户对应的权限
    // 3.封装成UserDetails对象，返回
    private Collection<? extends GrantedAuthority> getSysUserPermission(long id) {
        String roleCode = jdbcTemplate.queryForObject(LoginConstant.QUERY_ROLE_CODE_SQL, String.class, id);
        List<String> permissions = null;
        if (LoginConstant.ADMIN_ROLE_CODE.equals(roleCode)){
            // 超级管理员拥有所有权限数据
            permissions = jdbcTemplate.queryForList(LoginConstant.QUERY_ALL_PERMISSIONS, String.class);
        }else{
            // 普通用户 使用角色查询权限数据
            permissions = jdbcTemplate.queryForList(LoginConstant.QUERY_PERMISSION_SQL, String.class);
        }
        if (permissions == null || permissions.isEmpty()){
            return Collections.emptySet();
        }
        return permissions.stream().distinct()// 去重
                                    .map(perm->new SimpleGrantedAuthority(perm))
                                    .collect(Collectors.toSet());
    }

    /**
     * 普通用户登录
     * username可能是手机或邮箱，两种同时查询取并集结果，得到[1+0=1]或[0+0=0]的结果
     */
    private UserDetails loadMemberUserByUsername(String username) {
        return jdbcTemplate.queryForObject(LoginConstant.QUERY_MEMBER_SQL, new RowMapper<User>() {
            @Override
            public User mapRow(ResultSet rs, int i) throws SQLException {
                if (rs.wasNull()){
                    throw new UsernameNotFoundException("用户不存在:" + username);
                }
                long id = rs.getLong("id");
                String password = rs.getString("password");
                int status = rs.getInt("status");
                return new User(
                        String.valueOf(id),
                        // 这里返回响应的password需要是已加密过的，否则报错：Encoded password does not look like BCrypt。
                        // 应改为：encode之后保存到数据库，而不是数据库取出再encode。
                        passwordEncoder.encode(password),
                        status == 1,
                        true,
                        true,
                        true,
                        Arrays.asList(new SimpleGrantedAuthority(LoginConstant.USER_ROLE_CODE))
                );
            }
        }, username, username);
    }
}
