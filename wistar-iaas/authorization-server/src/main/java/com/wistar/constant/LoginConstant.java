package com.wistar.constant;

/**
 * 登录常量
 */
public class LoginConstant {
    /* 后台管理人员 */
    public static final String ADMIN_TYPE = "admin_type";
    /* 普通用户 */
    public static final String MEMBER_TYPE = "member_type";
    /* token的刷新 */
    public static final String  REFRESH_TOKEN= "refresh_token";
    /* 超级管理员的角色code */
    public static final String  ADMIN_ROLE_CODE= "ROLE_ADMIN";
    /* 普通用户的角色code */
    public static final String  USER_ROLE_CODE= "ROLE_USER";

    /**
     * 使用用户名查询用户
     */
    public static final String QUERY_ADMIN_SQL =
            "select id, username, password, status from sys_user where username = ?";
    /**
     * 查询用户的角色code
     */
    public static final String QUERY_ROLE_CODE_SQL=
            "select code from sys_role left join sys_user_role on sys_role.id=sys_user_role.role_id where sys_user_role.user_id=?";
    /**
     * 查询所有权限名称
     */
    public static final String QUERY_ALL_PERMISSIONS=
            "select name from sys_privilege";
    /**
     * 对于普通用户，我们需要先查询role->permissionId->permission
     */
    public static final String QUERY_PERMISSION_SQL =
            "select name from sys_privilege left join sys_role_privilege on sys_role_privilege.privilege_id = sys_privilege.id left join sys_user_role on sys_role_privilege.role_id = sys_user_role.role_id where sys_user_role.user_id = ?";
    /**
     * 普通用户查询sql
     */
    public static final String QUERY_MEMBER_SQL =
            "select id, password, status from user where mobile = ? or email = ?";

    /**
     * 使用id查询管理员的sql
     */
    public static final String QUERY_ADMIN_WITH_ID_SQL =
            "select username from sys_user where id = ?";
    /**
     * 使用id查询普通用户的sql
     */
    public static final String QUERY_MEMBER_WITH_ID_SQL =
            "select mobile from user where id = ?";
}
