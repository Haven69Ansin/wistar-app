package com.wistar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuthorizationApplication {

    // 获取token地址：  http://localhost:9999/oauth/token
    public static void main(String[] args){
        SpringApplication.run(AuthorizationApplication.class, args);
    }
}
