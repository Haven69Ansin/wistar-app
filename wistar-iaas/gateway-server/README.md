

###### 网关维度
网关访问某服务时的限流规则 
粒度粗-全局配置

###### API分组维度
网关访问某API接口时的限流规则 
粒度细-局部配置

###### 启动 sentinel-dashboard 客户端，配置流控规则
java -jar sentinel-dashboard.jar -Dserver.port=8858