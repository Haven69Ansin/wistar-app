package com.wistar.controller;

import com.alibaba.fastjson.JSONObject;
import com.wistar.model.LoginResult;
import com.wistar.model.WebLog;
import com.wistar.resp.RespResult;
import com.wistar.service.SysLoginService;
import com.wistar.service.TestService;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@RestController
@Api(tags = "管理后台-测试接口")
public class TestController {
    @Autowired
    private SysLoginService sysLoginService;
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    @Autowired
    private TestService testService;

    @GetMapping("/common/test")
    @ApiOperation(value = "测试方法", authorizations = {@Authorization("Authorization")})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "param1", value = "参数1", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "param2", value = "参数2", dataType = "String", paramType = "query")
    })
    public RespResult<List<String>> testMethod(String param1, String param2){
        List<String> data = new ArrayList<>(8);
        data.add(param1);
        data.add(param2);
        return RespResult.success(data);
    }

    @GetMapping("/date/test")
    @ApiOperation(value = "日期格式化-测试方法", authorizations = {@Authorization("Authorization")})
    public RespResult<Date> testDate(){
        return RespResult.success(new Date());
    }

    @GetMapping("/redis/test")
    @ApiOperation(value = "json格式化-测试方法", authorizations = {@Authorization("Authorization")})
    public RespResult<String> testRedis(){
        WebLog webLog = new WebLog();
        webLog.setResult("ok");
        webLog.setMethod("com.wistar.controller.testRedis");
        webLog.setUsername("1110");
        redisTemplate.opsForValue().set("com.wistar.weblog", webLog);
        return RespResult.success("ok");
    }

    @GetMapping("/jetcache/test")
    @ApiOperation(value = "jetcache缓存-测试方法", authorizations = {@Authorization("Authorization")})
    @ApiImplicitParam(name = "username", value = "用户名", dataType = "String", paramType = "query")
    public RespResult<String> testJetcache(String username){
        WebLog webLog = testService.get(username);
        log.info(JSONObject.toJSONString(webLog));
        return RespResult.success("ok");
    }

    @GetMapping("/login")
    @ApiOperation(value = "登录接口", authorizations = {@Authorization("Authorization")})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "用户名", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "password", value = "密码", dataType = "String", paramType = "query")
    })
    public RespResult<LoginResult> testLogin(String username, String password){
        return RespResult.success(sysLoginService.login(username, password));
    }
}
