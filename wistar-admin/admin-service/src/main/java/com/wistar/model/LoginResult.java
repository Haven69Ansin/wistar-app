package com.wistar.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginResult {

    private String token;
    private List<SysMenu> menus;
    private List<SimpleGrantedAuthority> authorities;
}
