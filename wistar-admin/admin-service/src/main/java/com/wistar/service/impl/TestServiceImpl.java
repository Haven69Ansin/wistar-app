package com.wistar.service.impl;

import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.wistar.model.WebLog;
import com.wistar.service.TestService;
import org.springframework.stereotype.Service;

@Service
public class TestServiceImpl implements TestService {

    /* 通过username，查询weblog */
    @Cached(name = "com.wistar.service.impl.TestServiceImpl",key = "#username",  cacheType = CacheType.BOTH)
    @Override
    public WebLog get(String username) {
        WebLog webLog = new WebLog();
        webLog.setUsername(username);
        webLog.setResult("ok");
        return webLog;
    }
}
