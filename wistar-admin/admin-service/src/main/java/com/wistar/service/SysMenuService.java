package com.wistar.service;

import com.wistar.model.SysMenu;

import java.util.List;

public interface SysMenuService {
    public List<SysMenu> getMenuByUserId(Long userId);
}
