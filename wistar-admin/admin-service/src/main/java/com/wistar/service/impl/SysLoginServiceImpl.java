package com.wistar.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.enums.ApiErrorCode;
import com.baomidou.mybatisplus.extension.exceptions.ApiException;
import com.wistar.feign.OAuth2FeignClient;
import com.wistar.model.JwtToken;
import com.wistar.model.LoginResult;
import com.wistar.model.SysMenu;
import com.wistar.service.SysLoginService;
import com.wistar.service.SysMenuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class SysLoginServiceImpl implements SysLoginService {
    @Autowired
    private OAuth2FeignClient oAuth2FeignClient;
    @Autowired
    private SysMenuService sysMenuService;
    @Value("${basic.token:Basic d2lzdGFyLWFwaTp3aXN0YXItc2VjcmV0}")
    private String basicToken;

    @Override
    public LoginResult login(String username, String password) {
        log.info("用户{}开始登录", username);
        // 1.远程调用authorization-server服务，获取token
        ResponseEntity<JwtToken> tokenResponseEntity = oAuth2FeignClient.getToken("password", "admin_type", username, password, basicToken);
        if (tokenResponseEntity.getStatusCode() != HttpStatus.OK){
            throw new ApiException(ApiErrorCode.FAILED);
        }
        JwtToken jwtToken = tokenResponseEntity.getBody();
        log.info("远程调用授权服务器成功，获取的token为{}", JSONObject.toJSONString(jwtToken, true));
        String token = jwtToken.getAccessToken();
        // 2.查询菜单数据
        Jwt jwt = JwtHelper.decode(token);
        String jwtJsonStr = jwt.getClaims();
        JSONObject jwtJson= JSON.parseObject(jwtJsonStr);
        Long userId = Long.valueOf(jwtJson.getString("user_name"));
        List<SysMenu> menus = sysMenuService.getMenuByUserId(userId);
        // 3.查询权限数据
        JSONArray  authoritiesJsonArray = jwtJson.getJSONArray("authorities");
        List<SimpleGrantedAuthority> authorities = authoritiesJsonArray.stream()// 组装权限数据
                .map(authorityJson->new SimpleGrantedAuthority(authorityJson.toString()))
                .collect(Collectors.toList());
        return new LoginResult(token, menus, authorities);
    }
}
