package com.wistar.service;

import com.wistar.model.LoginResult;

public interface SysLoginService {

    public LoginResult login(String username, String password);
}
