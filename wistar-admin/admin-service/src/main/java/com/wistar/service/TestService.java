package com.wistar.service;

import com.wistar.model.WebLog;

public interface TestService {

    /* 通过username，查询weblog */
    public WebLog get(String username);

}
