package com.wistar.feign;

import com.wistar.model.JwtToken;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "authorization-server")
public interface OAuth2FeignClient {

    @PostMapping("/oauth/token")
    public ResponseEntity<JwtToken> getToken(
            @RequestParam("grant_type") String grantType,// 授权类型：password 或 refresh_token
            @RequestParam("login_type") String loginType,// 登录类型：admin_type 或 member_type
            @RequestParam("username") String username,
            @RequestParam("password") String password,
            @RequestHeader("Authorization") String basicToken// Basic 由第三方客户端加密出现的值
    );
}
