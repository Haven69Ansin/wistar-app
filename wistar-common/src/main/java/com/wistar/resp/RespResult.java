package com.wistar.resp;


import com.alibaba.fastjson.JSON;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(
        value = "基础返回体",
        description = "基础返回体"
)
public class RespResult<T> implements Serializable {
    private static final long serialVersionUID = -8283555816173963054L;

    @ApiModelProperty(
            value = "状态码 0或者0000 为成功  其他均为失败",
            name = "code"
    )
    private String code = "0000";
    @ApiModelProperty(
            value = "返回消息",
            name = "msg"
    )
    private String msg;
    @ApiModelProperty(
            value = "业务返回体",
            name = "data"
    )
    private T data;

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return this.data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public boolean isSuccess(){
        return this.code.equals(RespCode.SUCCESS.getCode());
    }

    public RespResult() {
    }

    public RespResult(T data) {
        this.data = data;
        this.code = "0000";
        this.msg = "成功";
    }


    public RespResult(String code, String msg, T data) {
        this.data = data;
        this.code = code;
        this.msg = msg;
    }

    public RespResult(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static <T> RespResult<T> success(){
        RespResult<T> response = new RespResult<>();
        response.setCode(RespCode.SUCCESS.getCode());
        response.setMsg(RespCode.SUCCESS.getMsg());
        response.setData(null);
        return response;
    }

    public static <T> RespResult<T> success(T data){
        RespResult<T> response = new RespResult<>();
        response.setCode(RespCode.SUCCESS.getCode());
        response.setMsg(RespCode.SUCCESS.getMsg());
        response.setData(data);
        return response;
    }


    public static <T> RespResult<T> success(String msg,T data){
        RespResult<T> response = new RespResult<>();
        response.setData(data);
        response.setMsg(msg);
        response.setCode(RespCode.SUCCESS.getCode());
        return response;
    }

    public static <T> RespResult<T> error(String code,String msg){
        RespResult<T> response = new RespResult<>();
        response.setCode(code);
        response.setMsg(msg);
        response.setData(null);
        return response;
    }
    public static <T> RespResult<T> error(String code,String msg,T obj){
        RespResult<T> response = new RespResult<>();
        response.setCode(code);
        response.setMsg(msg);
        response.setData(obj);
        return response;
    }


    public static <T> RespResult<T> error(String msg){
        RespResult<T> response = new RespResult<>();
        response.setCode(RespCode.INTERNAL_SERVER_ERROR.getCode());
        response.setMsg(RespCode.INTERNAL_SERVER_ERROR.getMsg() + msg);
        response.setData(null);
        return response;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }

}