package com.wistar.resp;

public enum RespCode {
    SUCCESS("0000", "成功"),

    ERROR("1","接口请求失败"),
    INTERNAL_SERVER_ERROR("100", "服务器内部错误!"),


    NO_LOGIN("101", "请先登录！"),
    NO_REPEAT("102", "请勿重复请求！"),
    ILLEGAL_REQUEST("111", "非法请求！"),
    ACTIVITY_NO_START("121", "活动尚未开始!"),
    ACTIVITY_IS_END("122", "来晚了，当前活动已结束"),
    NO_TARGET_USER("130", "非目标用户"),
    RESOURCES_OUT("133", "活动太火爆，资源已用光"),

    GROUP_FULL("140", "来晚了，当前组织人数已满 "),
    GROUP_END("141", "来晚了，当前组织已结束 "),
    GROUP_JOINING("142", "您有正在参与的组织 "),
    GROUP_JOINED("143", "您已加入该组织，请勿重复加入 "),
    COMSUMER_JLJ_ERROR("142", "奖励金扣减失败 "),

    BLOCK_HANDLER("1001","活动火爆，请稍后重试"),
    FALLBACK("1002", "系统业务异常，请稍后重试"),


    ;
    private String code;
    private String msg;
    private RespCode(String code, String msg){
        this.code = code;
        this.msg = msg;
    }
    public String getCode(){
        return this.code;
    }
    public String getMsg(){
        return this.msg;
    }
}