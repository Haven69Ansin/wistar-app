package com.wistar.config.swagger;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;
import java.util.List;

@Configuration
@EnableSwagger2
//@Profile({"dev"})
@EnableConfigurationProperties(SwaggerProperties.class)
public class Swagger2Configuration {

    private SwaggerProperties swaggerProperties;

    public Swagger2Configuration(SwaggerProperties swaggerProperties){
        this.swaggerProperties = swaggerProperties;
    }

    @Bean
    public Docket createRestApi() {
        Docket docket = new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage(swaggerProperties.getBasePackage()))
                .paths(PathSelectors.any()) // 可以根据url路径设置哪些请求加入文档，忽略哪些请求
                .build();
        docket.securitySchemes(securitySchemes())// 安全规则
                .securityContexts(securityContexts());// 安全配置的上下文
        return docket;
    }

    /* 安全规则 */
    private List<? extends SecurityScheme> securitySchemes() {
        return Arrays.asList(new ApiKey("token", "Authorization", "Authorization"));
    }
    /* 安全上下文 */
    private List<SecurityContext> securityContexts() {
        return Arrays.asList(new SecurityContext(
                Arrays.asList(new SecurityReference("Authorization", new AuthorizationScope[]{new AuthorizationScope("global","accessResource")})),
                PathSelectors.any()
        ));
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .contact(new Contact(
                        swaggerProperties.getName(),
                        swaggerProperties.getUrl(),
                        swaggerProperties.getEmail()
                ))
                .title(swaggerProperties.getTitle()) //设置文档的标题
                .description(swaggerProperties.getDescription()) // 设置文档的描述
                .version(swaggerProperties.getTitle()) // 设置文档的版本信息
                .termsOfServiceUrl(swaggerProperties.getTermsOfServiceUrl())
                .build();
    }
}