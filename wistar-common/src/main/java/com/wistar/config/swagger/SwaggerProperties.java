package com.wistar.config.swagger;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "swagger2")
public class SwaggerProperties {
    /* 包扫描路径 */
    private String basePackage;
    /* 联系人名称 */
    private String name;
    /* 联系人主页 */
    private String url;
    /* 联系人邮箱 */
    private String email;
    /* 服务标题 */
    private String title;
    /* 服务描述 */
    private String description;
    /* 服务版本 */
    private String version;
    /* 服务团队 */
    private String termsOfServiceUrl;
}
