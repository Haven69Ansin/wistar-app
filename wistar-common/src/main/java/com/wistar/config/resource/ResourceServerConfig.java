package com.wistar.config.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.util.FileCopyUtils;

@EnableResourceServer
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Configuration
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

//    @Autowired
//    private UserDetailsService userDetailsService;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                .cors()// 开启跨域
                .and().csrf().disable()
                .sessionManagement().disable()// 关闭会话管理
                // .sessionCreationPolicy(SessionCreationPolicy.STATELESS)//Spring Security不会创建HttpSession，也不会使用它获取SecurityContext
                .authorizeRequests()
                .antMatchers(// 放行的资源路径，不需要token
                        "/login",
                        "/doc.html",
                        "/swagger-resources/**",
                        "/v2/**",
                        "/favicon.ico",
                        "/service-worker.js",
                        "/webjars/**"
                ).permitAll()
                .antMatchers(HttpMethod.OPTIONS).permitAll()// options 方法的请求放行
                .antMatchers("/**").authenticated()
                .and()
                // 这一步，告诉Security 框架，我们要用自己的UserDetailsService实现类
                // 来传递UserDetails对象给框架，框架会把这些信息生成Authorization对象使用
                // .userDetailsService(userDetailsService)
                .headers().cacheControl();
    }

    /**
     * 设置公钥
     * @param resources
     * @throws Exception
     */
    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.tokenStore(jwtTokenstore());
    }

    private TokenStore jwtTokenstore() {
        JwtTokenStore jwtTokenStore = new JwtTokenStore(accessTokenConverter());
        return jwtTokenStore;
    }

    @Bean// 放到ioc容器的
    public JwtAccessTokenConverter accessTokenConverter() {
        // resource 验证token(公钥), authorization 产生token(私钥)
        JwtAccessTokenConverter tokenConverter = new JwtAccessTokenConverter();
        ClassPathResource classPathResource = new ClassPathResource("wistarapp.txt");
        String str = null;
        try {
            byte[] bytes = FileCopyUtils.copyToByteArray(classPathResource.getInputStream());
            str = new String(bytes, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        tokenConverter.setVerifierKey(str);
        return tokenConverter;
    }
}
