package com.wistar.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/* web 操作日志记录 */
/* elk 整合 */
@Data
@EqualsAndHashCode(callSuper = false)
public class WebLog implements Serializable {
    /* 操作描述 */
    private String description;
    /* 操作用户 */
    private String username;
    /* 消耗时间 */
    private Integer spendTime;
    /* 根路径 */
    private String basePath;
    /* URI */
    private String uri;
    /* url */
    private String url;
    /* 请求方法 */
    private String method;
    /* ip 地址 */
    private String ip;
    /* 请求参数 */
    private Object parameter;
    /* 返回结果 */
    private Object result;

}
