package com.wistar.aspect;

import com.baomidou.mybatisplus.extension.api.IErrorCode;
import com.baomidou.mybatisplus.extension.exceptions.ApiException;
import com.wistar.resp.RespResult;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

    /* 内部api调用 的异常处理 */
    @ExceptionHandler(value = ApiException.class)
    public RespResult handleApiException(ApiException e){
        IErrorCode errorCode = e.getErrorCode();
        if (errorCode != null){
            return RespResult.error(String.valueOf(errorCode.getCode()), errorCode.getMsg());
        }
        return RespResult.error(e.getMessage());
    }

    /* 方法参数校验失败 的异常处理 */
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public RespResult handleMethodArgumentNotValidException(MethodArgumentNotValidException e){
        BindingResult bindingResult = e.getBindingResult();
        if (bindingResult.hasErrors()){
            FieldError fieldError = bindingResult.getFieldError();
            if (fieldError != null){
                RespResult.error(fieldError.getField()+fieldError.getDefaultMessage());
            }
        }
        return RespResult.error(e.getMessage());
    }

    /* 对象内部使用validate 没有校验成功 的异常处理 */
    @ExceptionHandler(value = BindException.class)
    public RespResult handleBindException(BindException e){
        BindingResult bindingResult = e.getBindingResult();
        if (bindingResult.hasErrors()){
            FieldError fieldError = bindingResult.getFieldError();
            if (fieldError != null){
                RespResult.error(fieldError.getField()+fieldError.getDefaultMessage());
            }
        }
        return RespResult.error(e.getMessage());
    }


}