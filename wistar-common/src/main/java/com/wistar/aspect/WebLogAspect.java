package com.wistar.aspect;

import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.URLUtil;
import com.wistar.model.WebLog;
import io.swagger.annotations.ApiOperation;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
import org.springframework.core.annotation.Order;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

@Component
@Aspect
@Order(1)
public class WebLogAspect {
    /* 日志记录：环绕通知：方法执行之前和之后 */

    /* 1.定义切入点 */
    /* controller 包里的所有类、所有方法、所有参数， 都有该切面 */
    @Pointcut("execution(* com.wistar.controller.*.*(..))")
    public void webLog(){

    }

    /* 2.记录日志的环绕通知 */
    @Around("webLog()")
    public Object recordWebLog(ProceedingJoinPoint joinPoint) throws Throwable {
        Object result = null;
        WebLog webLog = new WebLog();
        long start = System.currentTimeMillis();
        // 执行方法的真实调用
        result = joinPoint.proceed(joinPoint.getArgs());
        long end = System.currentTimeMillis();
        webLog.setSpendTime((int)(start-end)/1000);// 请求花费时间
        /* 获取当前的 request对象 */
        ServletRequestAttributes attributes = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        String url = request.getRequestURL().toString();
        webLog.setUri(request.getRequestURI());
        webLog.setUrl(url);
        /* http://ip:port/ */
        webLog.setBasePath(StrUtil.removeSuffix(url, URLUtil.url(url).getPath()));

        /* 获取安全的上下文 */
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        webLog.setUsername(authentication == null? "anonymous" : authentication.getPrincipal().toString());// 获取用户id
        webLog.setIp(request.getRemoteAddr());// todo 获取IP地址

        MethodSignature signature = (MethodSignature)joinPoint.getSignature();
        String targetClassName = joinPoint.getTarget().getClass().getName();
        Method method = signature.getMethod();
        /* 使用swagger工具， 在方法上添加@ApiOperation(value="")的注解 */
        ApiOperation apiOperationAnnotation = method.getAnnotation(ApiOperation.class);
        webLog.setDescription(apiOperationAnnotation == null?"no desc" : apiOperationAnnotation.value());
        webLog.setMethod(targetClassName+"."+method.getName());
        webLog.setParameter(getMethodParameter(method, joinPoint.getArgs()));
        webLog.setResult(result);
        return result;
    }

    /* 获取参数的执行参数 */
    // {"key_参数名称":"value_参数值"}
    private Object getMethodParameter(Method method, Object[] args) {
        Map<String, Object> paramWithValues = new HashMap<>();
        /* 获取形参名称  */
        LocalVariableTableParameterNameDiscoverer parameterNameDiscoverer =
                new LocalVariableTableParameterNameDiscoverer();
        String[] paramNames = parameterNameDiscoverer.getParameterNames(method);
        for (int i = 0; i < paramNames.length; i++) {
            paramWithValues.put(paramNames[i], args[i]);
        }
        return paramWithValues;
    }
}
