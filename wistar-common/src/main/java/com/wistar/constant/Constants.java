package com.wistar.constant;

public class Constants {
    public static final String UTF8 = "utf-8";
    public static final String GBK = "GBK";

    public static final String HTTP = "http://";
    public static final String HTTPS = "https://";

    /* 验证码 */
    public static final String CAPTCHA_CODE_KEY = "captcha_codes";
    public static final long CAPTCHA_EXPIRATION = 2;
}
