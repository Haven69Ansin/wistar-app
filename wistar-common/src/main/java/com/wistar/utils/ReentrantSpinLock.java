package com.wistar.utils;

public class ReentrantSpinLock extends SpinLock{

    private Thread owner;// 持有锁的线程
    private int count;// 重入次数

    @Override
    public void lock(){
        if (owner == null || owner != Thread.currentThread()){
            while (!this.value.compareAndSet(0,1));
            this.owner = Thread.currentThread();
        }
        count++;
    }

    public void unlock(){
        if (count == 1){
            this.value.compareAndSet(1,0);
        }
        count--;
    }
}
