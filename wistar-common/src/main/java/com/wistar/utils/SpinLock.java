package com.wistar.utils;

import java.util.concurrent.atomic.AtomicInteger;

public class SpinLock {
    /* 0-未上锁，1-上锁 */
     protected AtomicInteger value;

     public SpinLock(){
         this.value = new AtomicInteger();
         this.value.set(0);
     }

     public void lock(){
         // 自旋操作
         while(!this.value.compareAndSet(0, 1)){}
     }

     public void unlock(){
         // 将已上锁的解锁
         this.value.compareAndSet(1, 0);
     }
}
